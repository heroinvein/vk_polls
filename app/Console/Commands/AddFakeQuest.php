<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Artisan;
use Faker\Factory as Faker;

//require_once 'Faker/src/autoload.php';

class AddFakeQuest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quest:add {quantity} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add new quest(s) in database from table questions';
    
    protected $type;
    protected $counter;
    protected $faker;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = Faker::create('ru_RU');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->type = $this->argument('type');
        $ltype = $this->type;
        $this->counter = $this->argument('quantity');
        $i = $this->counter;
        while($i>0) {
            $this->info('type = '.$this->type.': '.$this->faker->$ltype);
            $i--;
        }
        $this->info('count - '.$this->counter);

	Artisan::call('db:seed', [
		'--class' => 'CreateQuests'
	]);
    }
}
