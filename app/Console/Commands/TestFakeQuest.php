<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Faker\Factory as Faker;

//require_once 'Faker/src/autoload.php';

class TestFakeQuest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quest:test {quantity} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'test data in faker';
    
    protected $type;
    protected $counter;
    protected $faker;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = Faker::create('ru_RU');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->type = $this->argument('type');
        $ltype = $this->type;
        $this->counter = $this->argument('quantity');
        $i = $this->counter;
        while($i>0) {
            $string = strval($this->faker->$ltype);
            $this->info('type = '.$this->type.': '.$string);
            $i--;
        }
    }
}
