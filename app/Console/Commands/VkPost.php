<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\VkApi;

class VkPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vk:post {count?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $count;
    protected $vk_api;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	$this->vk_api = new VkApi();
        $this->info('Started..');
	
	$this->argument('count') != '' ? $this->count = $this->argument('count') : $this->count = 1;
	
	$this->vk_api->run($this->count);
    }
}
