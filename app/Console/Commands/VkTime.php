<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class VkTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vk:time {time?} {dimension?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $dimension;
	protected $time;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->time = $this->argument('time'); 
        $this->dimension = $this->argument('dimension'); 
        switch($this->dimension) {
        	case 'month':
        		$this->time = $this->time * 2629743;
        	break;
        	
        	case 'week':
        		$this->time = $this->time * 604800;
        	break;
        	
        	case 'day':
        		$this->time = $this->time * 86400;
        	break;
        	
        	case 'hour':
        		$this->time = $this->time * 3600;
        	break;
        	
        	case 'minute':
        		$this->time = $this->time * 60;
        	break;
        	
        	default:
        		$this->time = $this->time * 1;
        	break;
        }
        	
        if(!empty($this->time)) {
        	DB::table('vk_api_config')->where('Id', 1)->update(['Span' => $this->time]);
        	$this->info('Время публикаций обновлено, новое значение (unix time): '.$this->time);
        }
        else {
        	$vkApiConfig = DB::table('vk_api_config')->where('Id', 1)->first();
        	$this->info('Текущее значение времени публикаций (unix time): '.$vkApiConfig->Span);
        }
    }
}
