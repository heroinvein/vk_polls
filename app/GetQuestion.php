<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class GetQuestion {
	
	protected $span;
	protected $output;
	
	public function __construct() {
	
		$this->output = new ConsoleOutput;
		$this->span = DB::table('vk_api_config')->where('id', 1)->first()->Span;
		
	}
	
	function get() {
	
		$nowTime = time();
		$max = DB::table('questions')->count();
		$pack = new \stdClass();
		$count = $max * 3;
		$bool = true;
		$rand;
		
		while($bool) {
			$count--;
			$rand = rand(1, $max);
			$questionTime = DB::table('poll_control_time')->where('id_question', $rand)->first();
			
			if(empty($questionTime)) {
				$bool = false;
				DB::table('poll_control_time')->insert(['id_question' => $rand, 
														'create_time' => $nowTime]);
			}
			elseif ($nowTime > ($questionTime->create_time + $this->span)) {
				$bool = false;
				DB::table('poll_control_time')->where('id_question', $rand)->update(['create_time' => $nowTime]);
			}
			elseif ($count <= 0) {
				$bool = false;
				$this->output->writeln('Похоже доступных для публикации опросов нет. Создайте новый или уменьшите временной диапазон публикации.');
				return '';
			}
		}
		
		$quest = DB::table('questions')->where('id', $rand)->first();
		$pack->quest = $quest;
		
		
		$counter = rand(2, 5);
		$position = rand(0, $counter);
		
		while($counter >= 0) {
			$rand = rand(1 , $max); 
			if($counter === $position) {
		    	$answer = DB::table('answers')->where('id', $quest->id)->first();
		    	$answer->text .= '_isTRUE';
		    	$answersArray[$counter] = $answer->text;
		    }
		    else {
		    	$answer = DB::table('answers')->where('id', $rand)->first();
		    	$answersArray[$counter] = $answer->text;
		    }
		    $counter--;
		}
		
		$pack->answers = json_encode($answersArray);

		return $pack;
		
	}

}
