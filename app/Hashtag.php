<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class Hashtag {
	
	protected $arrTag;
	protected $output;
	
	public function __construct() {
	
		$this->output = new ConsoleOutput;
		$this->arrTag = DB::table('hashtag')->get();
		
	}
	
	function get($code) {
		
		//var_dump($this->arrTag);
		
		foreach($this->arrTag as $v) {
			if($v->Code === $code) {
				return $v->Tag;
			}
		}
		
		return 'default@Tag';
		
	}

}
