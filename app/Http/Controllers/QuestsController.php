<?php

namespace App\Http\Controllers;

use App\Quests;
use Illuminate\Http\Request;

class QuestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quests = Quests::withCount('question')->get();

	return view('quests.index', compact('quests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quests  $quests
     * @return \Illuminate\Http\Response
     */
    public function show(Quests $quests)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quests  $quests
     * @return \Illuminate\Http\Response
     */
    public function edit(Quests $quests)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quests  $quests
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quests $quests)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quests  $quests
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quests $quests)
    {
        //
    }
}
