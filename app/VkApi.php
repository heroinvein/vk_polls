<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class VkApi {

	protected $vk_group;
	protected $vk_app_token;
	protected $vk_endpoint;
	protected $vk_version_api;

	protected $output;
	
	public function __construct() {
		$this->output = new ConsoleOutput();
		$config = DB::table('vk_api_config')->where('id', '1')->first();
		
	    $this->vk_group = $config->Group;
		$this->vk_app_token = $config->AppToken;
		$this->vk_endpoint = $config->Endpoint;
		$this->vk_version_api = $config->Version;
		
	}

	function run($count) {
		$getQuests = new GetQuestion;
		$Tag = new Hashtag;

		while($count > 0) {
			$poll = $getQuests->get();
			
			if(gettype($poll) == 'object') {

				$answersJSON = $poll->answers;
			
				$pollCreate = VkApi::vkApi_poll($poll->quest->name, $answersJSON);
											
				$photoUpload = VkApi::vkApi_photo('cat.jpg');
				
				$attachArray = [$pollCreate, $photoUpload, 'http://proghub.ru'];
											
				if($pollCreate) {
					
					$code = json_decode($poll->quest->code);
					$tag = '';
					foreach($code as $v) {
						$tag .= '#'.$Tag->get($v).' ';
					} 

					$return = VkApi::vkApi_call('wall.post', ['owner_id' => $this->vk_group, 
												'from_group' => '1', 
												'message' => $tag.PHP_EOL.PHP_EOL.'www.proghub.ru',
												'attachments' => implode(', ', $attachArray)]);
					if($return) {
						$this->output->writeln(PHP_EOL.'Пост опубликован: wall-159102438_'.$return['post_id']);
						foreach($attachArray as $v) {
							$this->output->writeln('Прикрепленный материал: '.$v);
						}
					}
					else {
						break;
					}
				}
				else {
					break;
				}
			}
			else {
				break;
			}
			$count--;
		}
		
		if($count != 0) {
			$this->output->writeln('Не опубликовано записей: '.$count);
		}
	}
	
	function vkApi_call($method, $params = array()) {
		if(empty($params['access_token'])){
			$params['access_token'] = $this->vk_app_token;
		}
	
		$params['v'] = $this->vk_version_api;
		$query = http_build_query($params);
	  	$url = $this->vk_endpoint.$method.'?'.$query;
 	  	$curl = curl_init($url);
	  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	  	$json = curl_exec($curl);
	  	$error = curl_error($curl);
	  	if ($error) {
	    		$this->output->writeln($error);
	    		return false;
	  	}
	  	curl_close($curl);
	  	$response = json_decode($json, true);
	  	if (!$response || !isset($response['response'])) {
	    		$this->output->writeln($json);
	    		return false;
	  	}
  		return $response['response'];
	}
	
	function vkAPI_poll($question, $answersJSON) {
		$create = VkApi::vkApi_call('polls.create', ['owner_id' => $this->vk_group, 
											'question' => $question,
											'add_answers' => $answersJSON]);
		return 'poll'.$create['owner_id'].'_'.$create['id'];
	}
	
	function vkApi_photo($file) {
		$wallServer = VkApi::vkApi_call('photos.getUploadServer', ['group_id' => $this->vk_group * -1,
															'album_id' => '250180018']);
		$upload = VkApi::vkApi_upload($wallServer['upload_url'], $file);
		$savePhoto = VkApi::vkApi_call('photos.save', ['group_id' => $this->vk_group * -1,
														'album_id' => $upload['aid'],
														'photos_list' => $upload['photos_list'],
														'server' => $upload['server'],
														'hash' => $upload['hash']
														]);
		return 'photo'.$savePhoto[0]['owner_id'].'_'.$savePhoto[0]['id'];
		
		}
		
	function vkApi_upload($url, $file_name) {
  		if (!file_exists($file_name)) {
    		$this->output->writeln('File not found: '.$file_name);
    		exit();
  		}
  		$curl = curl_init($url);
  		curl_setopt($curl, CURLOPT_POST, true);
  		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_POSTFIELDS, array('file' => new \CURLfile($file_name)));
  		$json = curl_exec($curl);
  		$error = curl_error($curl);
  		if ($error) {
    		$this->output->writeln($error);
    		return false;
  		}
  		curl_close($curl);
  		$response = json_decode($json, true);
  		if (!$response) {
    		$this->output->writeln("Invalid response for {$url} request");
    		return false;
  		}
  		return $response;
	}
}
