<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VkApiConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vk_api_config', function (Blueprint $table) {
            $table->increments('Id');
	    $table->text('Group');
	    $table->text('Span');
	    $table->text('Endpoint');
	    $table->text('Confirmation');
	    $table->text('Token');
	    $table->text('AppToken');
	    $table->text('Version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vk_api_config');
    }
}
