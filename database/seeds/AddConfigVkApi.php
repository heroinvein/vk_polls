<?php

use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Database\Seeder;

class AddConfigVkApi extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

	DB::table('vk_api_config')->insert([
            		'Group' => '-159102438',
			'Span' => '300',
			//'AppToken' => '3f0eb00c6c62c9b0c27d679ffbe9ce54f452b152caf91504b259370f2b42f14a640c8e0570c63d8469f05',
			'AppToken' => '928799ddf88f6503104e71bf4014b8204728afc6a53369bb20b096790301d25b6ea1cebc2848a1b149834',
			'Endpoint' => 'https://api.vk.com/method/',
            		'Confirmation' => '3d612ac1',
            		'Token' => 'bd6bd5c4c5efdf0a0917adedf7362c0eb9f1d3f64bb18411e17d7d8d5b2667ddb13bade55a4f8f6b6576d',
            		'Version' => '5.67'
        ]);


	$output->writeln('Insert config for vk api');
    }
}
