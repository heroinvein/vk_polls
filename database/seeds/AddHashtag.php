<?php

use Illuminate\Database\Seeder;

class AddHashtag extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$tag = ['cpp@quest', 'php@quest', 'jawa@quest', 'html@quest', 'cs@quest', 
		'ruby@quest', 'oop@quest', 'brainfuck@quest', 'js@quest', 'db@quest']; 
    
		$code = 1;
		foreach($tag as $v) {
        	DB::table('hashtag')->insert([
            					'code' => $code,
            					'tag' => $v
        						]);
        	$code++;					
        }
    }
}
