<?php

use Symfony\Component\Console\Output\ConsoleOutput;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CreateQuests extends Seeder
{
    //public function __construct($n) {
        
    //}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        $output = new ConsoleOutput();
	
        $i = 5;
        while($i>0) {
			$i--;
		
			$count = rand(1, 3);
			$j = 0;
			$arr = [];
			while($j < $count) {
				$c = rand(1, 10);
				$arr[$i] = $c ;
				$j++;
			}
			
			$code = json_encode($arr);
			
			$question_id =  DB::table('questions')->insertGetId([
    	        				'name' => $faker->text,
    	        				'image' => $faker->image('/tmp', 640, 480, 'cats', true, false),
    	        				'code' => $code,
    	        				'created_at' => $faker->date
    	    			]);
	
        	$answer_id = DB::table('answers')->insertGetId([
            			'question_id' => $question_id,
            			'text' => $faker->word,
            			'correct' => 1
        	     	     ]);

			$output->writeln('Create! => quests: '.$question_id.' | answer: '.$answer_id);
        }
    }
}
